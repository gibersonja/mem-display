package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path"
	"regexp"
	"runtime"
	"strconv"
	"strings"
)

type memoryAllocation struct {
	file              string
	addrStart         uint64
	addrStop          uint64
	addrSize          uint64 // Everything including this line to and including locked is in kB
	kernelPageSize    uint64
	mmuPageSize       uint64
	rss               uint64 // Amount of the mapping that is currently resident in RAM
	pss               uint64 // Process's proportional shareof this mapping
	sharedCleanPages  uint64
	sharedDirtyPages  uint64
	privateCleanPages uint64
	privateDirtyPages uint64
	referenced        uint64 // Amount of memory marked as referenced or accessed
	anonymous         uint64 // Amount of memory that does not belong to any file
	lazyFree          uint64 // Amount of memory that will be freed in memory pressure if the memory is clean
	anonHugePages     uint64 // Amount of memory backed by transparent hugepage
	shmemPmdMapped    uint64 // Amount of shared (shmem/tmpfs) memory backed by huge pages
	filePmdMapped     uint64 // Page cache mapped into userspace with huge pages
	sharedHugeTlb     uint64 // Amount of shared memory backed by hugetlbfs page that is not counted in RSS or PSS
	privateHugeTlb    uint64 // Amount of private memory backed by hugetlbfs page that is not counted in RSS or PSS
	swap              uint64 // Amount of would-be-anonymouse memory is also used, but out on swap
	swapPss           uint64 // Amount of proportional swap share of this mapping
	locked            uint64 // If mapping is locked in memory
	thpEligible       uint64 // Indicates whether the mapping is eligible for allocating THP pages
	vmFlags           uint64 // Kernel flags associated with the particular virtual memory area, use bitmap as follows:
}

// 0b0000000000000000000000000000001111111111111111111111111111111111
//	0b1 = rd = readable
//	0b10 = wr = writable
//	0b100 = ex = executable
//	0b1000 = sh = shared
//	0b10000 = mr = may read
//	0b100000 = mw = may write
//	0b1000000 = me = may execute
//	0b10000000 = ms = may share
//	0b100000000 = gd = stack segment growns down
//	0b1000000000 = pf = pure PFN range
//	0b10000000000 = dw = disabled write to the mapped file
//	0b100000000000 = lo = pages are locked in memory
//	0b1000000000000 = io = memory mapped I/O area
//	0b10000000000000 = sr = sequential read advise provided
//	0b100000000000000 = rr = random read advise provided
//	0b1000000000000000 = dc = do not copy area on fork
//	0b10000000000000000 = de = do not expand area on remapping
//	0b100000000000000000 = ac = area is accountable
//	0b1000000000000000000 = nr = swap space is not reserved for the area
//	0b10000000000000000000 = ht = area uses huge tlb pages
//	0b100000000000000000000 = sf = synchronous page fault
//	0b1000000000000000000000 = ar = architecture specific flag
//	0b10000000000000000000000 = wf = wipe on fork
//	0b100000000000000000000000 = dd = do not include area into core dump
//	0b1000000000000000000000000 = sd = soft dirty flag
//	0b10000000000000000000000000 = mm = mixed map area
//	0b100000000000000000000000000 = hg = huge page advise flag
//	0b1000000000000000000000000000 = nh = no huge page advise flag
//	0b10000000000000000000000000000 = mg = mergeable advise flag
//	0b100000000000000000000000000000 = bt = arm64 BTI guarded page
//	0b1000000000000000000000000000000 = mt = arm64 MTE allocation tags are enabled
//	0b10000000000000000000000000000000 = um = userfaultfd missing tracking
//	0b100000000000000000000000000000000 = uw = userfaultfd wr-protect tracking
//	0b1000000000000000000000000000000000 = ss = shadow stack page

type pid struct {
	name   string
	memory memoryAllocation
}

func main() {
	files, err := os.ReadDir("/proc")
	er(err)

	pids := make(map[int]pid)

	for _, file := range files {
		regex := regexp.MustCompile(`^\d+$`)
		if regex.MatchString(file.Name()) {
			pidnum, err := strconv.Atoi(file.Name())
			er(err)

			fh1, err := os.Open(fmt.Sprintf("/proc/%d/comm", pidnum))
			er(err)
			defer fh1.Close()

			var info pid

			scanner := bufio.NewScanner(fh1)
			for scanner.Scan() {
				info.name = strings.TrimSpace(scanner.Text())
			}

			pids[pidnum] = info

			fh2, err := os.Open(fmt.Sprintf("/proc/%d/smaps", pidnum))
			er(err)
			defer fh2.Close()

			var infoMem memoryAllocation
			var infoMemArray []string

			scanner = bufio.NewScanner(fh2)
			for scanner.Scan() {
				infoMemArray = append(infoMemArray, strings.TrimSpace(scanner.Text()))
			}

			for i := 0; i < len(infoMemArray); i++ {
				regex := regexp.MustCompile(`^([a-zA-z0-9]{8,16})-([a-zA-z0-9]{8,16})\s+\S+\s+\d+\s+\d+:\d+\s+\d+\s+(\S+)?\s*$`)
				if regex.MatchString(infoMemArray[i]) {
					hex := regex.FindStringSubmatch(infoMemArray[i])[1]
					/* EXAMPLE:
										numberStr := strings.Replace(s, "0x", "", -1)
					    numberStr = strings.Replace(numberStr, "0X", "", -1)
					    n, err := strconv.ParseUint(numberStr, 16, 64)
					    if err != nil {
					        panic(err)
					    }
					    return n
					*/
				}
			}

		}
	}
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(),
			path.Base(file), line, err)
	}
}
